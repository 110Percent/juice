import Command from '../class/command';
import { Message } from 'discord.js';
import { DiscordClient } from '../core/discord';
import { pull, plugins } from 'isomorphic-git';
import fs from 'fs';
import config from '../config/config';

class Upstart extends Command {
  constructor(client: DiscordClient) {
    super('upstart', client);
  }

  init() {
    this.logger.log('Initializing...');
    plugins.set('fs', fs);
  }

  async execute(msg: Message) {
    if (msg.author.id !== config.ownerID) {
      msg.channel.send('❌ Command must be used by the bot owner.');
      return;
    }
    const msg2 = (await msg.channel.send('⏬ Pulling from Git...')) as Message;
    await pull({
      dir: this.client.rootDir,
      ref: 'master',
      singleBranch: true
    });
    msg2.edit('✅ Pulled successfully. Restarting.');
  }
}

export default Upstart;
