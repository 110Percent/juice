import Command from '../class/command';
import { Message, RichEmbed } from 'discord.js';
import { DiscordClient } from '../core/discord';

class Snipe extends Command {
  constructor(client: DiscordClient) {
    super('snipe', client);
  }

  init() {
    this.logger.log('Initializing...');
  }

  execute(msg: Message) {
    let sniped: Message = this.client.sniper.pull(msg.channel.id);
    let embed: RichEmbed = new RichEmbed();

    embed
      .setAuthor(sniped.member.displayName, sniped.author.displayAvatarURL)
      .setDescription(sniped.content)
      .setTimestamp(sniped.createdAt);

    msg.channel.send(embed);
  }
}

export default Snipe;
