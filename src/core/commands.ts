import { readdirSync } from 'fs';
import { join } from 'path';
import Command from '../class/command';
import { Logger } from './logger';
import { Client } from 'discord.js';
import { DiscordClient } from './discord';

class Commands {
  client: Client;
  logger: Logger;
  commandPath: string;
  list: { [key: string]: Command };
  aliases: { [key: string]: Array<string> };

  constructor(cPath: string, client: DiscordClient) {
    this.client = client;
    this.logger = new Logger('Commands');
    this.commandPath = cPath;
    this.list = {};
    this.aliases = {};
    this.init();
  }

  init() {
    let files: Array<string> = readdirSync(
      join(__dirname, `../${this.commandPath}`)
    );
    files = files.filter(f => new RegExp('.+\\.js$').test(f));
    this.logger.log(`Files: ${files.join(', ')}`);
    for (let i in files) {
      let tmpBase = require(`../${this.commandPath}/${files[i]}`);
      let tmpCmd = new tmpBase(this.client);
      this.list[tmpCmd.name] = tmpCmd;
      if (tmpCmd.aliases) {
        this.aliases[tmpCmd.name] = tmpCmd.aliases;
      }
    }
  }
}

export default Commands;
