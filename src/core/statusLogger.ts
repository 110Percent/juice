import { DiscordClient } from './discord';

class StatusLogger {
  client: DiscordClient;
  db: any;

  constructor(client: DiscordClient, db: any) {
    this.client = client;
    this.db = db;

    this.client.on('presenceUpdate', (oMember, nMember) => {
      if (oMember.presence.status === nMember.presence.status) return;
      this.db.create({
        userID: nMember.user.id,
        status: nMember.presence.status
      });
    });
  }
}

export default StatusLogger;
