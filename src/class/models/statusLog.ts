import sequelize, { Sequelize, Model, InitOptions, BuildOptions } from "sequelize";

type StatusLogsStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): StatusLogs;
};

class StatusLogs extends Model {
  public userID: string;
  public status: string;
}

const init = (client: Sequelize) => {
  StatusLogs.init(
    {
      userID: {
        type: sequelize.STRING(20),
        allowNull: false
      },
      status: {
        type: sequelize.STRING(7),
        allowNull: false
      }
    },
    {
      tableName: 'statuslogs',
      sequelize: client
    }
  );
}

module.exports = { model: StatusLogs, init };
