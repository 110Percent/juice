const config = {
  token: '',
  prefix: '',
  ownerID: '',
  db: {
    host: '',
    username: '',
    pass: ''
  }
};

export default config;