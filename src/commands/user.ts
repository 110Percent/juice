import Command from '../class/command';
import { Message, GuildMember, RichEmbed } from 'discord.js';
import Fuse from 'fuse.js';
import moment from 'moment';
import { DiscordClient } from '../core/discord';

class UserCommand extends Command {
  constructor(client: DiscordClient) {
    super('user', client);
  }

  init() {
    this.logger.log('Initializing...');
  }

  execute(msg: Message, args: Array<string>) {
    if (args.length > 0) {
      const fuse = new Fuse(msg.guild.members.array(), {
        keys: ['user.username', 'nickname', 'id']
      });
      const results = fuse.search(args.join(' '));
      if (results.length > 0) {
        const embed: RichEmbed = this.process(results[0]);
        msg.channel.send(embed);
      } else {
        msg.channel.send('User not found...');
      }
    } else {
      const embed: RichEmbed = this.process(msg.member);
      msg.channel.send(embed);
    }
  }

  process(member: GuildMember) {
    const embed: RichEmbed = new RichEmbed();
    embed.setTitle(member.displayName);
    embed.setThumbnail(member.user.displayAvatarURL);

    const fields: string[] = [];

    fields.push(`▫️ **Tag**: ${member.user.tag}`);
    fields.push(`▫️ **ID**: ${member.user.id}`);
    if (member.nickname) fields.push(`▫️ **Nickname**: ${member.nickname}`);
    fields.push(
      `▫️ **Account Created:**: ${moment(
        member.user.createdTimestamp
      ).calendar()}`
    );
    fields.push(
      `▫️ **Joined Server**: ${moment(member.joinedTimestamp).calendar()}`
    );
    if (
      member.user.presence.game &&
      member.user.presence.game.name !== 'Custom Status'
    )
      fields.push(
        `▫️ **Currently playing**: ${member.user.presence.game.name}`
      );

    embed.setDescription(fields.join('\n'));

    return embed;
  }
}

export default UserCommand;
