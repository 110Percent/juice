import Command from '../class/command';
import { Message } from 'discord.js';
import { DiscordClient } from '../core/discord';
import { createCanvas, loadImage } from 'canvas';

class StatusRing extends Command {
  model: any;

  constructor(client: DiscordClient) {
    super('sr', client);
  }

  init() {
    this.logger.log('Initializing...');
  }

  async execute(msg: Message, args: Array<string>) {
    if (!this.model) {
      this.model = this.client.database.models.statusLog;
    }

    const colors: { [key: string]: string } = {
      offline: 'rgb(32, 34, 37)',
      online: 'rgb(67, 181, 129)',
      idle: 'rgb(250, 166, 26)',
      dnd: 'rgb(240, 71, 71)',
      background: 'rgb(54, 57, 63)'
    };

    let targetUser = msg.author.id;
    if (args && /\d{17}/.test(args[0])) {
      targetUser = args[0].match(/\d{17,}/)[0];
    }

    const entries = await this.model.findAll({
      where: { userID: targetUser }
    });

    const durations: { [key: string]: number } = { sum: 0 };

    if (entries.length > 0) {
      entries.sort((a: any, b: any) => a.dataValues.id - b.dataValues.id);

      // const periods = [];
      // let c = -1;
      for (let i = 1; i < entries.length; i++) {
        let p = entries[i - 1].dataValues;
        let e = entries[i].dataValues;
        if (p.createdAt === e.createdAt) continue;

        if (!durations[p.status]) {
          durations[p.status] = 0;
        }
        let duration =
          new Date(e.createdAt).valueOf() - new Date(p.createdAt).valueOf();
        if (Math.abs(duration) < 100) continue;
        durations[p.status] += duration;
        durations.sum += duration;
        /*
        if (p.status === e.status && i > 1) {
            periods[c].duration += new Date(e.createdAt).getTime() - new Date(p.createdAt).getTime();
        } else {
          periods.push({
            status: e.status,
            duration: new Date(e.createdAt).getTime() - new Date(p.createdAt).getTime() 
          });
          c++;
        }
      */
      }
      let e = entries[entries.length - 1].dataValues;

      if (!durations[e.status]) {
        durations[e.status] = 0;
      }
      let duration = new Date().getTime() - new Date(e.createdAt).getTime();
      durations[e.status] += duration;
      durations.sum += duration;
    } else {
      durations.sum = 1;
      durations[(await this.client.fetchUser(targetUser)).presence.status] = 1;
    }


    const { sum, ...statuses } = durations;

    const canvas = createCanvas(300, 300);
    const ctx = canvas.getContext('2d');
    ctx.strokeStyle = 'rgba(0, 0, 0, 0)';

    let startAngle = 0;
    let endAngle = 0;

    for (let i in statuses) {
      const thisAngle = (Math.PI * 2 * statuses[i]) / sum;
      startAngle = endAngle;
      endAngle += thisAngle;

      ctx.fillStyle = colors[i];
      ctx.beginPath();
      ctx.moveTo(150, 150);
      ctx.arc(150, 150, 150, startAngle, endAngle);
      ctx.lineTo(150, 150);
      ctx.stroke();
      ctx.fill();
      ctx.closePath();
    }

    ctx.fillStyle = colors.background;
    ctx.beginPath();
    ctx.moveTo(150, 150);
    ctx.arc(150, 150, 100, 0, Math.PI * 2);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();

    // Draw the circle avatar on top
    const avatar = await loadImage(
      (await this.client.fetchUser(targetUser)).displayAvatarURL
    );
    ctx.save();
    ctx.beginPath();
    ctx.arc(150, 150, 100, 0, 6.28, false);
    ctx.stroke();
    ctx.clip();
    ctx.drawImage(avatar, 50, 50, 200, 200);
    ctx.closePath();

    msg.channel.send({
      files: [{ attachment: canvas.toBuffer(), name: 'statusring.png' }]
    });
  }
}

export default StatusRing;
