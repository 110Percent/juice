import Command from '../class/command';
import { Message } from 'discord.js';
import { DiscordClient } from '../core/discord';

class Cry extends Command {
  letters: string;
  punctuation: string;

  constructor(client: DiscordClient) {
    super('cry', client);
    this.letters = 'abcdefghijklmnopqrstuvwxyz';
    this.punctuation = ';,,';
  }

  init() {
    this.logger.log('Initializing...');
  }

  execute(msg: Message, args: Array<string>) {
    let cryChars: Array<string> = args.join(' ').split('');
    for (let i = 0; i < 50; i++) {
      const roll = Math.floor(Math.random() * 100);
      if (roll < 1) {
        // Swap two letters
        const indices: Array<number> = [];
        for (let j = 0; j < 2; j++) {
          indices.push(Math.floor(Math.random() * cryChars.length));
          while (!cryChars[indices[j]].match(/[a-zA-Z]/) || indices.indexOf(indices[j]) < j) {
            indices[j] = Math.floor(Math.random() * cryChars.length);
          }
        }
        let tempChar = cryChars[indices[0]];
        cryChars[indices[0]] = cryChars[indices[1]];
        cryChars[indices[1]] = tempChar;
      } else if (roll < 2) {
        // Add a random letter
        const index = Math.floor(Math.random() * cryChars.length);
        cryChars.splice(index, 0, this.letters.charAt(Math.floor(Math.random() * this.letters.length)));
      } else if (roll < 5) {
        // Duplicate a letter
        const index = Math.floor(Math.random() * cryChars.length);
        cryChars.splice(index, 0, cryChars[index]);
      } else if (roll < 7) {
        // Add a comma or semicolon
        const index = Math.floor(Math.random() * cryChars.length);
        const mark = this.punctuation.charAt(Math.floor(Math.random() * this.punctuation.length));
        for (let j = Math.floor(Math.random() * 3); j < 3; j++) {
          cryChars.splice(index, 0, mark);
        }
      } else if (roll < 8) {
        // Change the case of a letter
        let index = Math.floor(Math.random() * cryChars.length);
        while (!cryChars[index].match(/[a-zA-Z]/)) {
          index = Math.floor(Math.random() * cryChars.length);
        }
        if (cryChars[index] === cryChars[index].toUpperCase()) {
          cryChars[index] = cryChars[index].toLowerCase();
        } else {
          cryChars[index] = cryChars[index].toUpperCase();
        }
      }
    }
    const cryString = cryChars.join('');
    msg.channel.send(cryString);
  }
}

export default Cry;
