import { Client, Message } from 'discord.js';
import { join } from 'path';
import { Logger } from './logger';
import config from '../config/config';
import Database from './database';
import MessageHandler from './handlers/message';
import Sniper from './sniper';
import StatusLogger from './statusLogger';

class DiscordClient extends Client {
  database: Database;
  logger: Logger;
  msgHandler: MessageHandler;
  rootDir: string;
  sniper: Sniper;
  statusLogger: StatusLogger;

  constructor() {
    super({
      disabledEvents: ['TYPING_START']
    });

    this.database = new Database();
    (async () => {
      await this.database.init();
      this.statusLogger = new StatusLogger(
        this,
        this.database.models.statusLog
      );
      console.log(this.database.models);
    })();
    this.logger = new Logger('Discord');
    this.sniper = new Sniper(this);
    this.msgHandler = new MessageHandler(this);
    this.rootDir = join(__dirname, '../../');
    this.login(config.token);

    this.on('ready', () => {
      this.logger.log(`Ready; Logged in as ${this.user.tag}`);
    });

    this.on('message', (msg: Message) => {
      this.msgHandler.handle(msg);
    });
  }
}

export { DiscordClient };
