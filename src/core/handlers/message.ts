import { Message } from 'discord.js';
import config from '../../config/config';
import CommandHandler from './command';
import { DiscordClient } from '../discord';

class MessageHandler {
  cmdHandler: CommandHandler;

  constructor(client: DiscordClient) {
    this.cmdHandler = new CommandHandler(client);
  }

  handle(msg: Message) {
    if (msg.content.startsWith(config.prefix)) {
      let toParse = msg.content.substr(config.prefix.length);
      let cmd = toParse.split(' ')[0];
      let args = toParse.split(' ');
      args.splice(0, 1);
      this.cmdHandler.handle(cmd, msg, args);
    }
  }
}

export default MessageHandler;
