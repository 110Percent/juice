import Command from '../class/command';
import { Message } from 'discord.js';
import { DiscordClient } from '../core/discord';

class Ping extends Command {
  constructor(client: DiscordClient) {
    super('ping', client);
  }

  init() {
    this.logger.log('Initializing...');
  }

  execute(msg: Message) {
    msg.channel.send('Pong');
  }
}

export default Ping;
