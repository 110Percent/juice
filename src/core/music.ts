import { EventEmitter } from 'events';
import {
  VoiceChannel,
  Guild,
  VoiceConnection,
  StreamDispatcher,
  TextChannel
} from 'discord.js';
import ytdl from 'ytdl-core';
import { Readable } from 'stream';

class Music {
  players: { [key: string]: Player };

  constructor() {
    this.players = {};
  }

  add(guild: Guild) {
    this.players[guild.id] = new Player(guild, this);
    return this.players[guild.id];
  }

  get(guild: Guild) {
    return this.players[guild.id] || this.add(guild);
  }
}

class Player extends EventEmitter {
  parentMusic: Music;
  playing: boolean;
  queue: Queue;
  guild: Guild;
  channel?: VoiceChannel;
  textChannel?: TextChannel;
  connection?: VoiceConnection;
  dispatcher?: StreamDispatcher;

  constructor(guild: Guild, parent: Music) {
    super();
    this.parentMusic = parent;
    this.playing = false;
    this.guild = guild;
    this.queue = new Queue();
  }

  async setChannels(channel: VoiceChannel, text: TextChannel) {
    this.channel = channel;
    this.textChannel = text;
    this.connection = await this.channel.join();
  }

  play() {
    this.playing = true;
    this.dispatcher = this.connection.playStream(
      this.queue.tracklist[0].load()
    );
    this.textChannel.send(
      `Now playing \`${this.queue.tracklist[0].title}\` in  🔊 \`${this.channel.name}\``
    );

    this.dispatcher.on('end', () => {
      this.queue.shift();
      this.playing = false;
      if (this.queue.tracklist.length > 0) {
        this.play();
      } else {
        this.channel.leave();
        this.kill();
      }
    });
  }

  skip() {
    this.dispatcher.end();
  }

  kill() {
    delete this.parentMusic.players[this.guild.id];
  }
}

class Queue extends EventEmitter {
  tracklist: Array<Song>;

  constructor() {
    super();
    this.tracklist = [];
  }

  add(url: string, source: string, title?: string) {
    this.tracklist.push(new Song(url, source, title));
  }

  shift() {
    this.tracklist.shift();
    this.emit('newSong');
  }

  remove(pos: number, amount: number = 1) {
    if (pos > 0 && amount > 0) {
      this.tracklist.splice(pos, amount);
    }
  }
}

class Song {
  url: string;
  title: string;
  source: string;
  stream: Readable;

  constructor(url: string, source: string, title?: string) {
    this.url = url;
    this.source = source;
    this.title = title || url.split('/')[url.split('/').length - 1];
    this.stream = new Readable();
  }

  load() {
    if (this.source == 'yt') {
      this.stream = ytdl(this.url, {
        filter: 'audioonly',
        highWaterMark: 1 << 25
      });
    }
    return this.stream;
  }
}

export default Music;
export { Player, Queue, Song };
