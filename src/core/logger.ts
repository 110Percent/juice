import chalk from 'chalk';

class Logger {
  name: string;
  constructor(name: string) {
    this.name = name;
  }

  log(msg: string) {
    console.log(`${chalk.bold(`[OUT][${this.name}]`)} ${msg}`);
  }

  err(msg: string) {
    console.log(`${chalk.bold(`${chalk.red('[ERR]')}[${this.name}]`)} ${msg}`);
  }
}

export { Logger };
