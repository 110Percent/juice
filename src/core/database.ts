import { Sequelize, Model } from 'sequelize';
import config from '../config/config';
import { readdirSync } from 'fs';
import { join } from 'path';
import { Logger } from './logger';

class Database {
  client: Sequelize;
  models: { [key: string]: any };
  logger: Logger;

  constructor() {
    const db = this;
    this.logger = new Logger('Database');

    const d = config.db;
    this.client = new Sequelize(
      `postgres://${d.username}:${d.pass}@${d.host}:5432/juice`,
      {
        logging: false
      }
    );

    this.models = {};
  }

  async init() {
    let modelFiles = readdirSync(join(__dirname, '../class/models')).filter(f =>
      f.endsWith('.js')
    );
    await this.client.authenticate();
    this.logger.log(`Successfully authenticated with Postgres!`);
    modelFiles.forEach(c => {
      const name = c.split('.')[0];
      const getModel = require(`../class/models/${c}`);
      getModel.init(this.client);
      this.models[name] = getModel.model;
      this.models[name].sync();
    });
  }
}

export default Database;
