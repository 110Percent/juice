import { Message } from 'discord.js';
import Commands from '../commands';
import Command from '../../class/command';
import { Logger } from '../logger';
import { DiscordClient } from '../discord';

class CommandHandler {
  commands: Commands;
  logger: Logger;

  constructor(client: DiscordClient) {
    this.logger = new Logger('CommandHandler');
    this.commands = new Commands('commands', client);
  }

  handle(cmd: string, msg: Message, args: Array<string>) {
    let command: Command;
    if ((command = this.commands.list[cmd])) {
      try {
        command.execute(msg, args);
        this.logger.log(`Executed command ${cmd}`);
      } catch (err) {
        this.logger.err(`Error executing command ${cmd}: ${err}`);
      }
    }
  }
}

export default CommandHandler;
