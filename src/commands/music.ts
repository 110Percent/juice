import Command from '../class/command';
import { Message, Guild, TextChannel } from 'discord.js';
import { getInfo } from 'ytdl-core';
import Music, { Player } from '../core/music';
import { DiscordClient } from '../core/discord';

class MusicCommand extends Command {
  music: Music;

  constructor(client: DiscordClient) {
    super('music', client);
    this.music = new Music();
  }

  init() {
    this.logger.log('Initializing...');
  }

  async execute(msg: Message, args: Array<string>) {
    let player: Player = this.music.get(msg.guild);
    if (args[0].toLowerCase() === 'play') {
      let query: string;
      if (args[0].match(new RegExp('@^https?://[^\\s/$.?#].[^\\s]*$@iS'))) {
        query = args[0];
      } else {
        let tempArgs: Array<string> = args;
        tempArgs.splice(0, 1);
        query = tempArgs.join(' ');
      }
      this.play(msg, player, query);
    } else if (args[0].toLowerCase() === 'skip') {
      player.skip();
      this.logger.log('Skipping');
    }
  }

  async play(msg: Message, player: Player, query: string) {
    let title: string | null = null;
    if (!msg.member.voiceChannel && !player.channel) {
      msg.channel.send('You must be in a voice channel to use this command!');
      return;
    }
    if (query.match(/^https?:\/\/(www.)?youtube\.com\/watch\?v=(.*)$/)) {
      this.logger.log(`Getting title for YT video ${query}`);
      title = (await getInfo(query)).title;
      this.logger.log(`Adding song ${title} to queue ${msg.guild.id}`);
      player.queue.add(query, 'yt', title);
      msg.channel.send(`Added \`${title}\` to the queue.`);
    }
    if (!player.channel) {
      await player.setChannels(
        msg.member.voiceChannel,
        msg.channel as TextChannel
      );
    }
    if (!player.playing) {
      player.play();
    }
  }
}

export default MusicCommand;
