import { Logger } from '../core/logger';
import { Message, Client } from 'discord.js';
import { DiscordClient } from '../core/discord';

class Command {

    client: DiscordClient;
    name: string;
    logger: Logger;

    constructor(name: string, client: DiscordClient) {
        this.name = name;
        this.logger = new Logger(name);
        this.client = client;
        this.init(client);
        this.logger.log(`Command ${this.name} ready`);
    }

    init(client: Client) { }

    execute(msg:Message, args:Array<string>) { }
}

export default Command;