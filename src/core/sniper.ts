import { Message } from 'discord.js';
import { DiscordClient } from './discord';

class Sniper {
  contents: { [key: string]: Message };
  constructor(client: DiscordClient) {
    this.contents = {};

    client.on('messageDelete', msg => {
      this.contents[msg.channel.id] = msg;
    });
  }

  pull(id: string) {
    return this.contents[id];
  }
}

export default Sniper;
